// eslint-disable-next-line import/prefer-default-export
export const notificationMessages = {
  error: 'No No No... NO!',
  success: 'Such success!',
  info: 'Some very useful info for you.',
  warning: 'You have been warned.'
};

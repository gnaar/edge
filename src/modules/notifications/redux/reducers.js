import { List } from 'immutable';
import { handleActions } from 'redux-actions';

export const ADD_NOTIFICATION = 'GNAR_EDGE_ADD_NOTIFICATION';
export const DISMISS_NOTIFICATION = 'GNAR_EDGE_DISMISS_NOTIFICATION';

let id = 0;

const reducers = {
  [ADD_NOTIFICATION]: (state, { payload }) => {
    const key = payload.key || `$$notification-${id++}$$`;
    return state.find(notification => notification.key === key) ? state : state.push({ key, ...payload });
  },
  [DISMISS_NOTIFICATION]: (state, { payload: { key } }) => state.filter(notification => notification.key !== key)
};

const initialState = () => List();

export default handleActions(reducers, initialState());

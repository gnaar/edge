import { LocalStorageMock } from 'mocks';
import { jwt24Jan69 } from 'mocks/constants';
import getJwt from 'modules/getJwt';

describe('getJwt', () => {
  it("returns null if the token doesn't exist in localStorage or can't be decoded", () => {
    expect(getJwt()).toBeNull();
  });

  it('returns the decoded jwt when localStorage contains a valid token with default localStorage keyName', () => {
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', jwt24Jan69);
    const jwt = getJwt();
    expect(jwt.exp).toEqual(new Date(Date.UTC(1969, 0, 24)).getTime() / 1000);
    localStorageMock.deactivate();
  });

  it('returns the decoded jwt when localStorage contains a valid token with custom localStorage keyName', () => {
    const keyName = 'canada';
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem(keyName, jwt24Jan69);
    const jwt = getJwt(keyName);
    expect(jwt.exp).toEqual(new Date(Date.UTC(1969, 0, 24)).getTime() / 1000);
    localStorageMock.deactivate();
  });
});

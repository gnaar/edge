import { createAction } from 'redux-actions';

import gnarActions from '../gnarActions';

describe('gnarActions', () => {
  it('creates actions with the correct names', () => {
    const WHISPER_IN_THE_WIND = 'WHISPER_IN_THE_WIND';
    const SUBMIT_ACTIVATION = 'SUBMIT_ACTIVATION';
    const SET_LANGUAGE = 'SET_LANGUAGE';
    const SUBMIT_LOGIN = 'SUBMIT_LOGIN';
    const ACTION_WITH_CUSTOM_PAYLOAD_CREATOR = 'ACTION_WITH_CUSTOM_PAYLOAD_CREATOR';
    const actions = gnarActions({
      testActions: {
        [WHISPER_IN_THE_WIND]: [],
        [SUBMIT_ACTIVATION]: 'token',
        [SET_LANGUAGE]: [ 'current' ],
        [SUBMIT_LOGIN]: [ 'email', 'password', 'reCaptchaResponse' ],
        [ACTION_WITH_CUSTOM_PAYLOAD_CREATOR]: price => ({ price: 2 * price })
      }
    });
    expect(Object.getOwnPropertyNames(actions)).toEqual([ 'testActions' ]);
    const { testActions } = actions;
    const actionNames =
      [ 'whisperInTheWind', 'submitActivation', 'setLanguage', 'submitLogin', 'actionWithCustomPayloadCreator' ];
    expect(Object.getOwnPropertyNames(testActions)).toEqual(actionNames);
  });

  it('correctly creates an action with no parameters', () => {
    const TEST_ACTION = 'TEST_ACTION';
    const actions = gnarActions({
      testActions: {
        [TEST_ACTION]: []
      }
    });
    const { testActions: { testAction } } = actions;
    expect(testAction()).toEqual({ type: TEST_ACTION, payload: {} });
    expect(testAction('test')).toEqual({ type: TEST_ACTION, payload: {} });
  });

  it('correctly creates an action with one string parameter', () => {
    const TEST_ACTION = 'TEST_ACTION';
    const actions = gnarActions({
      testActions: {
        [TEST_ACTION]: 'fruit'
      }
    });
    const { testActions: { testAction } } = actions;
    expect(testAction()).toEqual({ type: TEST_ACTION, payload: { first: undefined } });
    expect(testAction('apple')).toEqual({ type: TEST_ACTION, payload: { fruit: 'apple' } });
    expect(testAction('apple', 'tomato')).toEqual({ type: TEST_ACTION, payload: { fruit: 'apple' } });
  });

  it('correctly creates an action with one parameter in an array', () => {
    const TEST_ACTION = 'TEST_ACTION';
    const actions = gnarActions({
      testActions: {
        [TEST_ACTION]: [ 'fruit' ]
      }
    });
    const { testActions: { testAction } } = actions;
    expect(testAction()).toEqual({ type: TEST_ACTION, payload: { first: undefined } });
    expect(testAction('apple')).toEqual({ type: TEST_ACTION, payload: { fruit: 'apple' } });
    expect(testAction('apple', 'tomato')).toEqual({ type: TEST_ACTION, payload: { fruit: 'apple' } });
  });

  it('correctly creates an action with two parameters', () => {
    const TEST_ACTION = 'TEST_ACTION';
    const actions = gnarActions({
      testActions: {
        [TEST_ACTION]: [ 'fruit', 'vegetable' ]
      }
    });
    const { testActions: { testAction } } = actions;
    expect(testAction()).toEqual({ type: TEST_ACTION, payload: { fruit: undefined, vegetable: undefined } });
    expect(testAction('apple')).toEqual({ type: TEST_ACTION, payload: { fruit: 'apple', vegetable: undefined } });
    expect(testAction('apple', 'tomato'))
      .toEqual({ type: TEST_ACTION, payload: { fruit: 'apple', vegetable: 'tomato' } });
    expect(testAction('apple', 'tomato', 'platypus'))
      .toEqual({ type: TEST_ACTION, payload: { fruit: 'apple', vegetable: 'tomato' } });
  });

  it('correctly handles predefined actions', () => {
    const TEST_ACTION = 'TEST_ACTION';
    const SOME_OTHER_ACTION = 'SOME_OTHER_ACTION';
    const actions = gnarActions({
      testActions: {
        [TEST_ACTION]: 'fruit',
        someOtherAction: createAction(SOME_OTHER_ACTION, country => ({ country }))
      }
    });
    const { testActions: { testAction, someOtherAction } } = actions;
    expect(testAction('apple')).toEqual({ type: TEST_ACTION, payload: { fruit: 'apple' } });
    expect(someOtherAction('Canada')).toEqual({ type: SOME_OTHER_ACTION, payload: { country: 'Canada' } });
  });

  it('correctly handles predefined payload creators', () => {
    const ACTION_WITH_CUSTOM_PAYLOAD_CREATOR = 'ACTION_WITH_CUSTOM_PAYLOAD_CREATOR';
    const actions = gnarActions({
      testActions: {
        [ACTION_WITH_CUSTOM_PAYLOAD_CREATOR]: price => ({ price: 2 * price })
      }
    });
    const { testActions: { actionWithCustomPayloadCreator } } = actions;
    const action = { type: ACTION_WITH_CUSTOM_PAYLOAD_CREATOR, payload: { price: 200 } };
    expect(actionWithCustomPayloadCreator(100)).toEqual(action);
  });

  it('correctly handles nested actions', () => {
    const ACTION1 = 'ACTION1';
    const NESTED_ACTION1 = 'NESTED_ACTION1';
    const actions = gnarActions({
      testActions: {
        [ACTION1]: 'fruit',
        nestedActions: {
          [NESTED_ACTION1]: 'vegetable'
        }
      }
    });
    const { testActions: { action1, nestedActions: { nestedAction1 } } } = actions;
    expect(action1('apple')).toEqual({ type: ACTION1, payload: { fruit: 'apple' } });
    expect(nestedAction1('tomato')).toEqual({ type: NESTED_ACTION1, payload: { vegetable: 'tomato' } });
  });
});

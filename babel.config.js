module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/react'
  ],
  env: {
    production: {
      presets: [ 'react-optimize' ]
    }
  },
  plugins: [
    'react-hot-loader/babel',
    '@babel/transform-runtime',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-proposal-function-bind',
    '@babel/plugin-proposal-export-default-from',
    [ '@babel/plugin-proposal-decorators', { legacy: true } ],
    [ '@babel/plugin-proposal-class-properties', { loose: true } ],
    [ 'module-resolver', {
      root: [ './src' ],
      alias: {
        'gnar-redux': './src/modules/redux',
        mocks: './src/modules/__tests__/$mocks',
        modules: './src/modules',
        notifications: './src/modules/notifications',
        'notifications-mocks': './src/modules/notifications/__tests__/$mocks'
      }
    } ]
  ]
};
